#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define SIZE 256
#define THRESHOLD 50

// binarization処理する関数
void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }

  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      // binarization処理
      binarization(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  int x, y, color; // for文用の変数
  for (color = 0; color < 3; color++) {
    for (y = 0; y < SIZE; y++) {
      for (x = 0; x < SIZE; x++) {
        if(source[color][y][x] >= THRESHOLD) results[color][y][x] = 255;
        else results[color][y][x] = 0;
      }
    }
  }
}
