# Binarization
入力画像を2値化する  
- 入力画像  
![in](https://gitlab.com/ogahiro21/Binarization/raw/image/image/in15.jpeg)  
- 出力画像  
![out](https://gitlab.com/ogahiro21/Binarization/raw/image/image/out15.jpeg)

## Usage
**コンパイル**
```
gcc -o cpbmp binarization.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in15.bmp ans15.bmp
```
